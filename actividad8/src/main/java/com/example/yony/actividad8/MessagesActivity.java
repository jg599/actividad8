package com.example.yony.actividad8;

import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class MessagesActivity extends AppCompatActivity {
    MiIntentService intn =new MiIntentService();
    private ListView list;
    ArrayList<String> lista_mensajes =new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);
        DataHolder.instance.agregarReferenciaMessageActivity(this);
        list = (ListView)findViewById(R.id.listView);


        lista_mensajes.add(intn.msg);
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista_mensajes);
        list.setAdapter(adaptador);

    }


    public void cargarFicheroconDatos() {

        try {
            File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "mensajes.pmdm");
            FileInputStream fis = new FileInputStream(f);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader bf = new BufferedReader(isr);
            String sLine = null;
            while ((sLine = bf.readLine()) != null) {
               String[] sTokens = sLine.split("|");
                addMessageToList(sTokens[0], sTokens[1], sTokens[2]);
            }
            bf.close();
            isr.close();
            fis.close();
        } catch (Exception e) {

        }


    }

    public void guardarEnFichero() {

        try{
            File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "mensajes.pmdm");
            FileOutputStream fis = new FileOutputStream(f);
           OutputStreamWriter osr = new OutputStreamWriter(fis, "UTF-8");
            PrintWriter pw = new PrintWriter(osr);


          //  for (int i=0;i<msgs.size();i++){
              //  pw.println(msgs.get(i).msg + "|" + msgs.get(i).cid+"|"+msgs.get(i).user);
            //    pw.println(msgs.get(i));
           // }
pw.close();

            fis.close();

osr.close();

        }catch (Exception e){

        }
    }

    public void addMessageToList(String sToken, String token, String s){

    }
}




