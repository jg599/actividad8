package com.example.yony.actividad8;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;



public class MiWakefullBroadcastReceiver extends WakefulBroadcastReceiver {


    public static final int NOTIFICATION_ID=1;
    @Override
    public void onReceive(Context context, Intent intent) {
        //Log.v("GcmBroadcastReceiver", "HE RECIBIDO!!!!");
        // Explicitly specify that GcmIntentService will handle the intent.
        Intent i = new Intent(context,MainActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtras(intent.getExtras());
        context.startActivity(i);
        ComponentName comp = new ComponentName(context.getPackageName(), MiIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);

        enviar_notificacion(context,i);
    }

    private void enviar_notificacion(Context context,Intent intent){

        String msg=intent.getExtras().getString("name");

        NotificationManager notificationManager=(NotificationManager)context.getSystemService(context.NOTIFICATION_SERVICE);

        Intent i = new Intent(context,MainActivity.class);
        i.putExtras(intent.getExtras());
        PendingIntent contentIntent = PendingIntent.getActivity(context,0,i,0);



        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_utad)
                        .setContentTitle("Mensaje de JG")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg)
                .setAutoCancel(true);

        mBuilder.setContentIntent(contentIntent);
        notificationManager.notify(NOTIFICATION_ID,mBuilder.build());
    }
}
